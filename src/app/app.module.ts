import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReactiveFormsComponent } from './components/reactive-forms/reactive-forms.component';
import { ReactiveFormsValidationsComponent } from './components/reactive-forms-validations/reactive-forms-validations.component';
import { HeaderComponent } from './shared/header/header.component';
import { EmailValidatorDirective } from './shared/custom/email-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    ReactiveFormsComponent,
    ReactiveFormsValidationsComponent,
    HeaderComponent,
    EmailValidatorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
