import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsComponent } from './components/reactive-forms/reactive-forms.component';
import { ReactiveFormsValidationsComponent } from './components/reactive-forms-validations/reactive-forms-validations.component';

const routes: Routes = [
  {path: '', redirectTo: 'forms', pathMatch: 'full' },
  { path: 'forms', component: ReactiveFormsComponent },
  { path: 'forms-advanced', component: ReactiveFormsValidationsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
